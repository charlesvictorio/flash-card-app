#!/usr/bin/env python3

"""Generate an html file for the flash card app that has many different deck html files.

2022.10.24: and then launch!

TODO:
- add options
    - all decks in folder (with nesting). for now: `./make_session_deck.py ../cards/*/*`
    - `-o` specify output file
"""

import argparse
import os
import webbrowser

OUTPUT_FILE = os.path.join(os.path.dirname(__file__), "generated_by_app_each_time_it_is_run/index.html")

parser = argparse.ArgumentParser(description = "Generate an html file for the flash card app that has many different deck html files.")
parser.add_argument("files", nargs = "+")

args = parser.parse_args()

raw_text_of_arg_files = []
for fname in args.files:
    with open(fname, "r") as f:
        raw_text_of_arg_files.append(f.read())

one_big_string_of_raw_html = "".join(raw_text_of_arg_files)

with open(os.path.join(os.path.dirname(__file__), "template.html"), "r") as f:
    raw_text_of_template = f.read()

comment_delimiter = "<!-- This comment will only be shown on the template -->"

p1, p2 = raw_text_of_template.split(comment_delimiter)

generated_html_file = p1 + one_big_string_of_raw_html + p2

with open(OUTPUT_FILE, "w") as f:
    f.write(generated_html_file)

webbrowser.open(OUTPUT_FILE)
