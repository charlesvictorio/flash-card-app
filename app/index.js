// TODO: refactor into a state-based system

// Helper functions

function randInt(upper_bound) {
    // return an integer from [0, upper_bound)
    return Math.floor(Math.random() * upper_bound);
}

//function randCard(cards) {
    //var randomIndex = randInt(cards.length);
    //return cards[randomIndex];
//}

function hide_card(card) {
    card.style.display = "none";
    card.getElementsByClassName("front")[0].style.display = "none";
    card.getElementsByClassName("back")[0].style.display = "none";
}

// Main class
class App {
    constructor() {
        // HTML elements
        this.arena = document.getElementById("arena");
        this.content = document.getElementById("content");
        this.flip_btn = document.getElementById("flip_btn");
        this.good_btn = document.getElementById("good_btn");
        this.bad_btn = document.getElementById("bad_btn");
        this.again_btn = document.getElementById("again_btn");
        this.line = document.createElement("hr");

        // Stuff that should be JS objects but are HTML elements
        // I am making the good and bad piles html elements instead of js arrays.
        // I don't think this is how you are supposed to do it.
        this.unseen_pile = document.getElementById("unseen_pile");
        this.good_pile = document.getElementById("good_pile");
        this.bad_pile = document.getElementById("bad_pile");

        // JS objects
        //this.unseen_cards_nodelist = this.unseen_pile.querySelectorAll("div.card"); // static, must manually update when unseen pile changes
        this.current_card = null;
        //this.cc_index = null;
        this.front = null;
        this.back = null;
    }

    unseen_pile_length() {
        return this.unseen_pile.querySelectorAll("div.card").length;
    }

    set_current_card(new_current_card) { // refactor into scctruc?
        this.current_card = new_current_card;
        this.front = new_current_card.getElementsByClassName("front")[0]; // I thought `element.getElementsByClassName("class")` wasn't supposed to work
        this.back = new_current_card.getElementsByClassName("back")[0];
    }

    set_current_card_to_random_unseen_card() {
        this.cc_index = randInt(this.unseen_pile_length());
        this.set_current_card(this.unseen_pile.querySelectorAll("div.card")[this.cc_index]);
    }

    //set_up() {
        //// Hide everything
        //// maybe make this default with css
        //for (const card of this.total_deck) {
            //hide_card(card);
        //}
        //this.flip_btn.style.display = "none";
        //this.good_btn.style.display = "none";
        //this.bad_btn.style.display = "none";
        //this.again_btn.style.display = "none";
    //}

    pick_and_show_new_unseen_card_front() { // pick and show
        //console.log(this.unseen_pile_length());
        if (this.unseen_pile_length() === 0) {
            this.deck_ended();
        } else {
            // pick a new current card
            this.set_current_card_to_random_unseen_card();
            this.content.appendChild(this.current_card); // Add it to the arena

            // Show the card (container div), front (text div), and flip button
            this.current_card.style.display = "block";
            this.front.style.display = "block";
            this.flip_btn.style.display = "inline";
        }
    }

    deck_ended() {
        // hide arena
        this.arena.style.display = "none";
        // can display good and bad piles to show what i got wrong, plus a do again button
        this.good_pile.style.display = "block";
        this.bad_pile.style.display = "block";
        this.again_btn.style.display = "inline";
        this.line.style.display = "none";
    }

    play_again() {
        // Undo deck_ended
        this.again_btn.style.display = "none";
        this.good_pile.style.display = "none";
        this.bad_pile.style.display = "none";
        this.arena.style.display = "block";
        //this.line.style.display = "block"; // do this when flip

        // pour the good and bad piles into the unseen arr
        // and hide them
        for (const pile of [this.good_pile, this.bad_pile]) {
            for (const card of pile.querySelectorAll("div.card")) {
                hide_card(card);
                this.unseen_pile.appendChild(card);
            }
        }

        this.pick_and_show_new_unseen_card_front();
    }

    flip() {
        this.flip_btn.style.display = "none";
        this.good_btn.style.display = "inline";
        this.bad_btn.style.display = "inline";
        this.front.after(this.line);
        this.line.style.display = "block";
        this.back.style.display = "block";
    }

    next_card(good) {
        // // remove current card from unseen pile
        // this.unseen_arr.splice(this.cc_index, 1);
        
        // move current card from arena to good or bad pile 
        if (good) {
            this.good_pile.appendChild(this.current_card);
        } else {
            this.bad_pile.appendChild(this.current_card);
        }

        // hide good and bad btns
        this.good_btn.style.display = "none";
        this.bad_btn.style.display = "none";
        // pick and show next cc front
        this.pick_and_show_new_unseen_card_front();

    }

    go() {
        //this.set_up(); // everything is hidden using css files
        this.pick_and_show_new_unseen_card_front();
    }

}

var app = new App();

app.go();
